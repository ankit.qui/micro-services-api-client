# Summary
- as per our need we have mutiliple microservices that interact with each other and manage the data with diffrent allocated services.
- so we have to create docker network that share among all containers.
- generate api client that interact with user services throgh profile services. here we need not write manual requests code. here we have to just call auto generated python client code which intract with other targeted services.

# steps 
- 1 - go to profile services run command `docker-compose up --build `
- 2 - go to user services services run below command to gerate api_client - (Make sure your profile services are running)
`docker run --rm --network="microservices" -v ${PWD}:/local openapitools/openapi-generator-cli generate   -i http://profileweb:8000/swagger.yaml   -g python   -o /local/out/python `
- 3 - go to user services run command `docker-compose up --build `

``` python
from openapi_client.api.profile_api import ProfileApi   # this is auto geneated client based on our rest api schema. 
profile_api = ProfileApi()
profile_api.create_profile(data={'email': 'abc@abc.com'})   # behind the scene this will call profile service create profile api 
profile_api.profile_list()   # this will called profile list api
```

# External Tool
- For generating api client https://github.com/OpenAPITools/openapi-generator

- The API documents are at:
  - http://127.0.0.1:8876/swagger/
   
- Apis that are interacting user services to profile services are:
  - http://127.0.0.1:8877/profile-create
  - http://127.0.0.1:8877/profile-list
  - http://127.0.0.1:8877/profile-update

# Note 
```
- as of the testing criteria put all the data as static data 
- we can modify the APis and all the validations as per need
- as of now we are just going to check [create, upadte and list operations]
```

