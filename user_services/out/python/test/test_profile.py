"""
    Test API

    React Crud API's  # noqa: E501

    The version of the OpenAPI document: v1
    Contact: contact@snippets.local
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.profile import Profile


class TestProfile(unittest.TestCase):
    """Profile unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testProfile(self):
        """Test Profile"""
        # FIXME: construct object with mandatory attributes with example values
        # model = Profile()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
