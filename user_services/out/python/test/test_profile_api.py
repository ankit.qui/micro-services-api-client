"""
    Test API

    React Crud API's  # noqa: E501

    The version of the OpenAPI document: v1
    Contact: contact@snippets.local
    Generated by: https://openapi-generator.tech
"""


import unittest

import openapi_client
from openapi_client.api.profile_api import ProfileApi  # noqa: E501


class TestProfileApi(unittest.TestCase):
    """ProfileApi unit test stubs"""

    def setUp(self):
        self.api = ProfileApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_profile_create(self):
        """Test case for profile_create

        """
        pass

    def test_profile_delete(self):
        """Test case for profile_delete

        """
        pass

    def test_profile_list(self):
        """Test case for profile_list

        """
        pass

    def test_profile_partial_update(self):
        """Test case for profile_partial_update

        """
        pass

    def test_profile_read(self):
        """Test case for profile_read

        """
        pass

    def test_profile_update(self):
        """Test case for profile_update

        """
        pass


if __name__ == '__main__':
    unittest.main()
