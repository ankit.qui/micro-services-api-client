# Profile


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** |  | 
**id** | **int** |  | [optional] [readonly] 
**name** | **str, none_type** |  | [optional] 
**password** | **str, none_type** |  | [optional] 
**city** | **str, none_type** |  | [optional] 
**date** | **date, none_type** |  | [optional] 
**state** | **str, none_type** |  | [optional] 
**age** | **int, none_type** |  | [optional] 
**address** | **str, none_type** |  | [optional] 
**image** | **str, none_type** |  | [optional] [readonly] 
**favorite_colour** | **str, none_type** |  | [optional] 
**status** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


