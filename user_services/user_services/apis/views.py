from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from openapi_client.api.profile_api import ProfileApi
import uuid


# Create your views here.

class UserProfileViewSet(GenericViewSet):
    """
        here is the APis to perform the crud operation on Profile models.
    """
    request_data = {
        "name": "test user",
        "password": "test@123",
        "city": "unknown",
        "state": "unknown",
        "age": 22,
        "address": "unknown",
        "favorite_colour": "unknown",
        "status": False

    }
    client = ProfileApi()  # api client that interact to Profile model APi

    def profile_create(self, request, *args, **kwargs):
        """ create profile model data through api client"""
        self.request_data["email"] = self.get_email()
        response = self.client.profile_create(data=self.request_data)
        return Response({"response": response.to_dict()})

    def get_email(self):
        email_name = uuid.uuid1()
        return f"{str(email_name)}@gmail.com"

    def profile_list(self, request, *args, **kwargs):
        """ get list of profile model data through api client"""
        response = self.client.profile_list()
        json_response = [each.to_dict() for each in response]
        return Response({"response": json_response})

    def profile_update(self, request, *args, **kwargs):
        """ update the profile model data through api client"""
        self.request_data["email"] = self.get_email()
        self.request_data["name"] = "user updated"
        self.request_data["city"] = "city updated"
        try:
            response = self.client.profile_update(id=1, data=self.request_data)
        except:
            response = self.client.profile_create(data=self.request_data)
        return Response({"response": response.to_dict()})
