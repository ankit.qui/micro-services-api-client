from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import UserProfileViewSet


urlpatterns = [
        path("profile-create",  UserProfileViewSet.as_view({"post": "profile_create"})),
        path("profile-list",  UserProfileViewSet.as_view({"get": "profile_list"})),
        path("profile-update",  UserProfileViewSet.as_view({"patch": "profile_update"})),
    ]
