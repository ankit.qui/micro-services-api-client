from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser


class Profiles(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=15, null=True, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    state = models.CharField(max_length=50, null=True, blank=True)
    age = models.PositiveIntegerField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to="profiles/", null=True, blank=True)
    favorite_colour = models.CharField(null=True, blank=True, max_length=30)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.email
