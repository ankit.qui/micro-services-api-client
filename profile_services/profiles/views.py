from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Profiles
from .serializers import ProfileSerializer


# Create your views here.

class ProfileViewSet(ModelViewSet):
    """
        here is the APis to perform the crud operation on Profile models.
    """
    queryset = Profiles.objects.all()
    serializer_class = ProfileSerializer
    authentication_classes = []
    permission_classes = []
